const fs = require('fs')
const path = require('path')
const package = process.env.package

if (typeof package !== 'string') {
  throw new Error('Must provide package name')
}

const local = path.join(__dirname, '..', 'local')
const manifest = path.join(local, package, 'monolith.json')

if (fs.existsSync(manifest)) {
  const config = JSON.parse(fs.readFileSync(manifest))

  if (config.local) {
    Object.keys(config.local).forEach(namespace => {
      const packages = config.local[namespace]

      if (typeof packages === 'object') {
        Object.keys(packages).forEach(packageName => {
          console.log(`${namespace}/${packageName}`)
        })
      }
    })
  }
}
