# monolith

The Monolith

## About

The Monolith is an object of reflection and attention that absorbs responsibility from those who align themselves with it, so they can offload basic tasks to the Monolith.

## Development

Basic setup:

- clone this repository
- `npm install`
- make changes
- `bin/status`
- `bin/add`
- `bin/commit`
- `bin/push`

To install a package, use

`package=react/button npm run local:install`

To remove a package, use

`package=react/button npm run local:rm`

To be able to run react examples, install the react example package

`package=react/example npm run local:install`

To run an example, use `package=react/example npm run local:run -- react/button`
